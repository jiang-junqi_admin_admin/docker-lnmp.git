1. docker-compose.yaml 文件。
2. 包含 nginx + php + mysql + redis + rabbitMq 容器 数据文件+配置文件挂载。
3. 文件目录安装按照当前格式 勿更改 .env文件配置CONF_DIR为docker-compose.yaml目录的绝对路径。目录不可带有中文，新建的目录也一样。
4. php安装swoole扩展，后续可以直接创建hyperf框架。
5. php文件夹内使用Dockerfile构建php扩展。
6. 有时候安装amqp扩展出了点问题，具体情况我也没找到，用了一些翻墙工具又是正常的。不需要amqp扩展的话，在php的Dockerfile中删除即可。
7. 容器端口映射和mysql密码，mq用户名密码自行在.env文件中配置。

### 容器基本命令
docker desktop for windows自动带有docker-compose，linux上的需要自行去安装docker-compose
```
docker-compose up -d 后台启动
docker-compose down 删除
docker-compose build <容器名> 构建镜像 构建完成后需要down然后up一下
```

将项目放在 www 目录下面即可

新建php项目则增加 在nginx/conf/conf.d目录下www.example.com.conf 文件，例：
```
server
{
    listen 80;
    server_name  www.example.com;  #host文件同步
    location / {
        root /usr/share/nginx/html/; #后面加上项目入口文件路径
        index  index.html index.htm index.php;
        location / {  #nginx伪静态设置 url去掉index.php
            try_files $uri $uri/ /index.php?$query_string;
        }
        autoindex off;
    }
    location ~ \.php$ {
        root /var/www/html/;     #后面加上项目入口文件路径
        fastcgi_pass php81:9000; #更改php81，容器名称是啥就改啥
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
    #反向代理设置
    location /swoole/ { #需要在swoole后面加上 / 才能匹配所有路由
        proxy_pass http://php:9501/; #也需要加 /
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        #支持websocket链接
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }
    #增加支持socket.io
    location ^~/socket.io/ {
        # 执行代理访问真实服务器
        proxy_pass http://php:9501; #不需要加 / 折磨了我一天   具体什么情况加 什么情况不加 暂不明白
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```